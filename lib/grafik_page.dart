import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:test_aegis_2/models/model_home.dart';

class GrafikPage extends StatefulWidget {
  const GrafikPage({super.key, required this.token});
  final String? token;
  @override
  State<GrafikPage> createState() => _GrafikPageState();
}

class _GrafikPageState extends State<GrafikPage> {
  int minYear = 2020;
  int maxYear = 2022;

  @override
  void initState() {
    getPieData(widget.token!);
    getLineData(widget.token!);
    getStackedData(widget.token!);
    super.initState();
  }

  void getFilteredData() {
    getPieData(widget.token!, minYear, maxYear);
    getLineData(widget.token!, minYear, maxYear);
    getStackedData(widget.token!, minYear, maxYear);
  }

  List<PieChartData> pieChartData = [];
  void getPieData(String token, [int? minYearFilter, int? maxYearFilter]) {
    pieChartData.clear();
    PieChartModel.getData(token, minYearFilter, maxYearFilter).then((value) {
      for (int i = 0; i < value.length; i++) {
        pieChartData.add(PieChartData(value['label'][i], value['data'][i]));
      }
      setState(() {});
    });
  }

  List<LineChartData> lineChartData = [];
  void getLineData(String token, [int? minYearFilter, int? maxYearFilter]) {
    lineChartData.clear();
    LineChartModel.getData(token, minYearFilter, maxYearFilter).then((value) {
      for (int i = 0; i < value['period'].length; i++) {
        lineChartData.add(LineChartData(value['period'][i], value['data'][i]));
      }
      setState(() {});
    });
  }

  List<CartesianSeries> stackedChartSeries = [];
  List<StackedChartData> stackedChartData = [];

  void getStackedData(String token, [int? minYearFilter, int? maxYearFilter]) {
    stackedChartData.clear();
    stackedChartSeries.clear();
    int valueMale = 0;
    int valueFemale = 0;
    StackedChartModel.getData(token, minYearFilter, maxYearFilter)
        .then((value) {
      for (int i = 0; i < value['period'].length; i++) {
        valueMale = value['data'][0]['data'][i];
        valueFemale = value['data'][1]['data'][i];
        stackedChartData
            .add(StackedChartData(value['period'][i], valueMale, valueFemale));
      }
      for (int i = 0; i < value['data'].length; i++) {
        stackedChartSeries.add(
          ColumnSeries<StackedChartData, int>(
            name: value['data'][i]['name'],
            dataSource: stackedChartData,
            xValueMapper: (StackedChartData data, _) => data.xAxis,
            yValueMapper: (StackedChartData data, _) =>
                (i > 0) ? data.yAxis2 : data.yAxis,
          ),
        );
      }
      setState(() {});
    });
  }

  InkWell pickYearButton(bool isMinYear) {
    return InkWell(
      onTap: () {
        DatePicker.showDatePicker(context,
            showTitleActions: true,
            minTime: DateTime(2015),
            maxTime: DateTime.now(), onConfirm: (date) {
          setState(() {
            log('change ' + date.year.toString());
            (isMinYear) ? minYear = date.year : maxYear = date.year;
            (minYear > maxYear) ? maxYear = DateTime.now().year : null;
          });
        }, currentTime: DateTime.now(), locale: LocaleType.id);
      },
      child: Container(
        padding: const EdgeInsets.all(5.0),
        color: Colors.grey.shade300,
        child: Text(
          isMinYear ? minYear.toString() : maxYear.toString(),
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(10),
      child: ListView(
        children: [
          Container(
            margin: EdgeInsets.all(10),
            child: Column(
              children: [
                Card(
                  margin: EdgeInsets.all(10),
                  child: Padding(
                    padding: const EdgeInsets.all(10),
                    child: Row(
                      children: [
                        Text("Filter: "),
                        Spacer(),
                        pickYearButton(true),
                        Spacer(),
                        Text("to"),
                        Spacer(),
                        pickYearButton(false),
                        Spacer(),
                        ElevatedButton(
                            onPressed: () {
                              getFilteredData();
                            },
                            child: Text("Filter"))
                      ],
                    ),
                  ),
                ),
                Card(
                  margin: EdgeInsets.all(10),
                  child: SfCircularChart(
                    title: ChartTitle(
                      text: "Jenis Kelamin Pegawai",
                    ),
                    legend: Legend(
                        isVisible: true,
                        overflowMode: LegendItemOverflowMode.scroll),
                    tooltipBehavior: TooltipBehavior(enable: true),
                    series: <CircularSeries>[
                      PieSeries<PieChartData, String>(
                          dataSource: pieChartData,
                          xValueMapper: (PieChartData data, _) => data.xAxis,
                          yValueMapper: (PieChartData data, _) => data.yAxis,
                          explode: true,
                          explodeIndex: 1)
                    ],
                  ),
                ),
                Card(
                  margin: EdgeInsets.all(10),
                  child: SfCartesianChart(
                    primaryXAxis: NumericAxis(decimalPlaces: 0),
                    title: ChartTitle(
                      text: "Jumlah Pegawai",
                    ),
                    tooltipBehavior: TooltipBehavior(enable: true),
                    series: <ChartSeries>[
                      LineSeries<LineChartData, int>(
                        name: "Jumlah Pegawai",
                        dataSource: lineChartData,
                        xValueMapper: (LineChartData data, _) => data.xAxis,
                        yValueMapper: (LineChartData data, _) => data.yAxis,
                      )
                    ],
                  ),
                ),
                Card(
                  margin: EdgeInsets.all(10),
                  child: SfCartesianChart(
                    primaryXAxis: CategoryAxis(),
                    title: ChartTitle(
                      text: "Jumlah Pegawai",
                    ),
                    legend: Legend(
                        isVisible: true,
                        overflowMode: LegendItemOverflowMode.scroll,
                        position: LegendPosition.bottom),
                    tooltipBehavior: TooltipBehavior(enable: true),
                    series: stackedChartSeries,
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

class PieChartData {
  PieChartData(this.xAxis, this.yAxis);
  final String xAxis;
  final int yAxis;
}

class LineChartData {
  LineChartData(this.xAxis, this.yAxis);
  final int xAxis;
  final int yAxis;
}

class StackedChartData {
  StackedChartData(this.xAxis, this.yAxis, this.yAxis2);
  final int xAxis;
  final int yAxis;
  final int yAxis2;
}
