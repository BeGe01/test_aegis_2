import 'dart:convert';
import 'dart:developer';

import "package:http/http.dart" as http;

class Pegawai {
  int? id;
  String? firstName;
  String? lastName;
  String? email;

  bool? status;
  Map<String, dynamic>? result;

  Pegawai({this.id, this.firstName, this.lastName, this.email});

  factory Pegawai.createList(Map<String, dynamic> object) {
    return Pegawai(
        id: object['id'],
        firstName: object['first_name'],
        lastName: object['last_name'],
        email: object['email']);
  }

  static Future<List<Pegawai>> getData(
      int limit, int page, String token) async {
    String apiURL =
        "https://test-magang.aegis.co.id/pegawai?limit=$limit&page=$page";

    var apiResult = await http.get(Uri.parse(apiURL), headers: {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "Bearer " + token
    });
    var jsonObject = json.decode(apiResult.body);
    List<dynamic> listPegawai =
        (jsonObject as Map<String, dynamic>)['result']['items'];
    // log(apiResult.body);

    List<Pegawai> pegawai = [];
    for (int i = 0; i < listPegawai.length; i++)
      pegawai.add(Pegawai.createList(listPegawai[i]));
    return pegawai;
  }
}

class PieChartModel {
  static Future<Map<String, dynamic>> getData(String token,
      [int? minYearFilter, int? maxYearFilter]) async {
    String apiURL = (minYearFilter == null)
        ? "https://test-magang.aegis.co.id/pegawai/pie_chart"
        : "https://test-magang.aegis.co.id/pegawai/pie_chart?min_year=$minYearFilter&max_year=$maxYearFilter";

    var apiResult = await http.get(Uri.parse(apiURL), headers: {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "Bearer " + token
    });
    var jsonObject = json.decode(apiResult.body);
    Map<String, dynamic> listPieData =
        (jsonObject as Map<String, dynamic>)['result'];
    // log(apiResult.body);

    Map<String, dynamic> pieData = {};
    for (int x = 0; x < listPieData.length; x++) {
      String currentKey = listPieData.keys.elementAt(x).toString();
      List dataList = [];
      for (int y = 0; y < listPieData[currentKey].length; y++) {
        dataList.add(listPieData[currentKey][y]);
      }
      pieData.addAll({currentKey: dataList});
    }
    // inspect(pieData);
    // log(pieData['label'][1]);
    return pieData;
  }
}

class LineChartModel {
  static Future<Map<String, dynamic>> getData(String token,
      [int? minYearFilter, int? maxYearFilter]) async {
    String apiURL = (minYearFilter == null)
        ? "https://test-magang.aegis.co.id/pegawai/line_chart"
        : "https://test-magang.aegis.co.id/pegawai/line_chart?min_year=$minYearFilter&max_year=$maxYearFilter";

    var apiResult = await http.get(Uri.parse(apiURL), headers: {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "Bearer " + token
    });
    var jsonObject = json.decode(apiResult.body);
    Map<String, dynamic> listLineData =
        (jsonObject as Map<String, dynamic>)['result'];
    // log(apiResult.body);

    Map<String, dynamic> lineData = {};
    for (int x = 0; x < listLineData.length; x++) {
      String currentKey = listLineData.keys.elementAt(x).toString();
      List dataList = [];
      for (int y = 0; y < listLineData[currentKey].length; y++) {
        dataList.add(listLineData[currentKey][y]);
      }
      lineData.addAll({currentKey: dataList});
    }
    // inspect(lineData);
    return lineData;
  }
}

class StackedChartModel {
  static Future<Map<String, dynamic>> getData(String token,
      [int? minYearFilter, int? maxYearFilter]) async {
    String apiURL = (minYearFilter == null)
        ? "https://test-magang.aegis.co.id/pegawai/stacked_chart"
        : "https://test-magang.aegis.co.id/pegawai/stacked_chart?min_year=$minYearFilter&max_year=$maxYearFilter";

    var apiResult = await http.get(Uri.parse(apiURL), headers: {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "Bearer " + token
    });
    var jsonObject = json.decode(apiResult.body);
    Map<String, dynamic> listStackedData =
        (jsonObject as Map<String, dynamic>)['result'];
    // log(apiResult.body);

    Map<String, dynamic> stackedData = {};
    for (int x = 0; x < listStackedData.length; x++) {
      String currentKey = listStackedData.keys.elementAt(x).toString();
      List dataList = [];
      for (int y = 0; y < listStackedData[currentKey].length; y++) {
        dataList.add(listStackedData[currentKey][y]);
      }
      stackedData.addAll({currentKey: dataList});
    }
    inspect(stackedData['data']);
    return stackedData;
  }
}
