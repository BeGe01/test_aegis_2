import 'dart:convert';
import 'dart:developer';

import "package:http/http.dart" as http;

class Login {
  bool? status;
  String? token;
  Map<String, dynamic>? result;
  String? errorMessage;

  Login({this.status, this.token, this.result, this.errorMessage});

  factory Login.loginResult(Map<String, dynamic> object) {
    object['status']
        ? log(object['result']['token'].toString())
        : log(object['result']);

    return Login(
        status: (object['status']),
        token: (object['status']) ? object['result']['token'].toString() : null,
        result: (object['status']) ? object['result'] : null,
        errorMessage: (object['status']) ? null : object['result']);
  }

  static Future<Login> login(String email, String password) async {
    String apiURL = "https://test-magang.aegis.co.id/login";

    var apiResult = await http.post(Uri.parse(apiURL),
        body: jsonEncode({"email": email, "password": password}),
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json"
        });
    var jsonObject = json.decode(apiResult.body);

    return Login.loginResult(jsonObject);
  }

  static void logout(String token) async {
    String apiURL = "https://test-magang.aegis.co.id/logout";

    var apiResult = await http.post(Uri.parse(apiURL), headers: {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "Bearer " + token
    });
  }
}
