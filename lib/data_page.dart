import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:numberpicker/numberpicker.dart';
import 'package:test_aegis_2/models/model_home.dart';

class DataPage extends StatefulWidget {
  DataPage({Key? key, required this.token}) : super(key: key);
  final String? token;
  @override
  State<DataPage> createState() => _DataPageState();
}

class _DataPageState extends State<DataPage> {
  @override
  void initState() {
    getDataPegawai(10, 1, widget.token!);
    super.initState();
  }

  List<Widget> dataPegawai = [];
  void getDataPegawai(
    int limit,
    int page,
    String token,
  ) {
    // List<Widget> dataPegawai = [];
    Pegawai.getData(limit, page, widget.token!).then((value) {
      dataPegawai.clear();
      for (int i = 0; i < value.length; i++) {
        setState(() {
          dataPegawai.add(Container(
            width: double.maxFinite,
            decoration: BoxDecoration(color: Colors.white, boxShadow: [
              BoxShadow(
                color: Colors.grey.shade300,
                offset: Offset(0, 2),
                blurRadius: 3,
                spreadRadius: 2,
              )
            ]),
            margin: EdgeInsets.all(10),
            child: Padding(
              padding: EdgeInsets.all(10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("ID: " + value[i].id.toString()),
                  Text("FIRST NAME: " + value[i].firstName.toString()),
                  Text("LAST NAME:" + value[i].lastName.toString()),
                  Text("EMAIL: " + value[i].email.toString())
                ],
              ),
            ),
          ));
        });
        // log(value[i].first_name.toString());
      }
    });
  }

  TextEditingController limit = TextEditingController(text: "10");
  TextEditingController page = TextEditingController(text: "1");

  Widget filterRowWidget() {
    return Row(
      children: [
        Text("Limit: "),
        Spacer(),
        Expanded(
          child: TextField(
            textAlign: TextAlign.center,
            controller: limit,
          ),
        ),
        Spacer(),
        Text("Page: "),
        Spacer(),
        Expanded(
          child: TextField(
            textAlign: TextAlign.center,
            controller: page,
          ),
        ),
        Spacer(),
        ElevatedButton(
            onPressed: () {
              getDataPegawai(
                  int.parse(limit.text), int.parse(page.text), widget.token!);
            },
            child: Text("Submit"))
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    inspect(dataPegawai);
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      padding: EdgeInsets.all(10),
      child: ListView(children: [
        (dataPegawai.isNotEmpty)
            ? Column(
                children: [
                  Container(
                    width: double.maxFinite,
                    child: Card(
                      margin: EdgeInsets.all(10),
                      child: Padding(
                          padding: const EdgeInsets.all(10),
                          child: filterRowWidget()),
                    ),
                  ),
                  Container(
                    width: double.maxFinite,
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: dataPegawai),
                  ),
                ],
              )
            : Center(
                child: CircularProgressIndicator(),
              )
      ]),
    );
  }
}
