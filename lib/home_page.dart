import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:test_aegis_2/data_page.dart';
import 'package:test_aegis_2/grafik_page.dart';
import 'package:test_aegis_2/login_page.dart';
import 'package:test_aegis_2/models/model_login.dart';

class HomePage extends StatefulWidget {
  HomePage({Key? key, required this.token}) : super(key: key);
  final String? token;

  @override
  State<HomePage> createState() => _HomePageState();
}

TabBar homeTabBar = TabBar(tabs: [
  Tab(
    icon: Icon(Icons.description_rounded),
    text: "Data",
  ),
  Tab(
    icon: Icon(Icons.leaderboard_rounded),
    text: "Grafik",
  )
]);

class _HomePageState extends State<HomePage> {
  void doLogout() async {
    Login.logout(widget.token!);
    SharedPreferences pref = await SharedPreferences.getInstance();
    setState(() {
      pref.remove("is_login");
      pref.remove("email");
      pref.remove("token");
    });
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) {
      return LoginPage();
    }));
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
          appBar: AppBar(
            title: Text("Home Page"),
            bottom: homeTabBar,
            actions: [
              IconButton(
                  onPressed: () {
                    doLogout();
                  },
                  icon: Icon(Icons.logout))
            ],
          ),
          body: TabBarView(children: [
            DataPage(token: widget.token),
            GrafikPage(
              token: widget.token,
            )
          ])),
    );
  }
}
