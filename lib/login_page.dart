import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:test_aegis_2/home_page.dart';
import 'package:email_validator/email_validator.dart';
import 'package:test_aegis_2/models/model_login.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController emailController = TextEditingController(text: null);
  TextEditingController passController = TextEditingController(text: null);
  bool _passwordVisible = false;

  SnackBar snackBar = SnackBar(
    content: Text('Email atau Password salah!'),
  );

  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    _passwordVisible = false;
    checkLogin();
  }

  void doLogin(String email, String password) async {
    Login.login(email, password).then((value) => value.status!
        ? saveSession(email, value.token!)
        : ScaffoldMessenger.of(context)
            .showSnackBar(SnackBar(content: Text(value.errorMessage!))));
  }

  saveSession(String email, String token) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    await pref.setString("email", email);
    await pref.setBool("is_login", true);
    await pref.setString("token", token);

    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) {
      return HomePage(
        token: token,
      );
    }));
  }

  void checkLogin() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    var islogin = pref.getBool("is_login");
    if (islogin != null && islogin) {
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) {
        return HomePage(
          token: pref.getString('token'),
        );
      }));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Center(
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset("assets/logo_lrs.png"),
                  Container(
                    margin: EdgeInsets.all(30),
                    child: Text(
                      "Sistem Informasi\nMarketing dan Sales",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 25,
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(5),
                    width: MediaQuery.of(context).size.width * 0.8,
                    child: TextFormField(
                        validator: (value) {
                          if (value == null ||
                              value.isEmpty ||
                              !EmailValidator.validate(value)) {
                            return 'Please enter an email';
                          }
                          return null;
                        },
                        controller: emailController,
                        decoration:
                            loginTextfieldDecoration(isPassword: false)),
                  ),
                  Container(
                    margin: EdgeInsets.all(5),
                    width: MediaQuery.of(context).size.width * 0.8,
                    child: TextFormField(
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Please enter a password';
                        }
                        return null;
                      },
                      controller: passController,
                      obscureText: !_passwordVisible,
                      decoration: loginTextfieldDecoration(isPassword: true),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(5),
                    child: ElevatedButton(
                        onPressed: () {
                          if (_formKey.currentState!.validate()) {
                            doLogin(emailController.text, passController.text);
                          }
                        },
                        child: Text(
                          "Submit",
                          style: TextStyle(fontSize: 15),
                        ),
                        style: ElevatedButton.styleFrom(
                            minimumSize: Size(
                                MediaQuery.of(context).size.width * 0.8, 40),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)))),
                  ),
                ],
              ),
            ),
          ),
        ));
  }

  InputDecoration loginTextfieldDecoration({required bool isPassword}) {
    return InputDecoration(
        labelText: isPassword ? "Password" : "Email",
        contentPadding: EdgeInsets.symmetric(vertical: 18, horizontal: 30),
        labelStyle:
            TextStyle(fontWeight: FontWeight.bold, color: Colors.grey.shade400),
        floatingLabelStyle: TextStyle(color: Colors.blue),
        enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
            borderSide: BorderSide(width: 3, color: Colors.grey.shade300)),
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
            borderSide: BorderSide(width: 3, color: Colors.blue)),
        errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
            borderSide: BorderSide(width: 3, color: Colors.red)),
        focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
            borderSide: BorderSide(width: 3, color: Colors.red)),
        filled: true,
        fillColor: Colors.transparent,
        suffixIcon: isPassword
            ? IconButton(
                onPressed: () {
                  setState(() {
                    _passwordVisible = !_passwordVisible;
                  });
                },
                icon: Icon(
                  _passwordVisible ? Icons.visibility : Icons.visibility_off,
                  color: Colors.grey.shade300,
                ))
            : null);
  }
}
